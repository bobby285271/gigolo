# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# samson <sambelet@yahoo.com>, 2016-2018
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-04 12:48+0100\n"
"PO-Revision-Date: 2021-03-04 11:48+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Amharic (http://www.transifex.com/xfce/xfce-apps/language/am/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: am\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../src/main.c:48
msgid "Connect all bookmarks marked as 'auto connect' and exit"
msgstr ""

#: ../src/main.c:49
msgid "Ignore running instances, enforce opening a new instance"
msgstr ""

#: ../src/main.c:50
msgid "Print a list of supported URI schemes"
msgstr ""

#: ../src/main.c:51
msgid "Be verbose"
msgstr ""

#: ../src/main.c:52
msgid "Show version information"
msgstr "የ እትም መረጃ ማሳያ"

#: ../src/main.c:124
msgid "- a simple frontend to easily connect to remote filesystems"
msgstr ""

#: ../src/common.c:77
msgid "Unix Device"
msgstr "Unix Device"

#: ../src/common.c:79
msgid "Windows Share"
msgstr ""

#: ../src/common.c:81
msgid "FTP"
msgstr "FTP"

#: ../src/common.c:83
msgid "HTTP"
msgstr "HTTP"

#: ../src/common.c:85
msgid "SSH"
msgstr "SSH"

#: ../src/common.c:87
msgid "Obex"
msgstr "Obex"

#: ../src/common.c:89
msgid "WebDAV"
msgstr "WebDAV"

#: ../src/common.c:91
msgid "WebDAV (secure)"
msgstr "WebDAV (secure)"

#: ../src/common.c:93 ../src/window.c:1598
msgid "Network"
msgstr "ኔትዎርክ"

#: ../src/common.c:95
msgid "Archive"
msgstr "ማህደር"

#: ../src/common.c:97
msgid "Photos"
msgstr "ፎቶዎች"

#: ../src/common.c:99
msgid "Custom Location"
msgstr "አካባቢ ማስተካከያ"

#: ../src/window.c:260
#, c-format
msgid "Connecting to \"%s\""
msgstr "በ መገናኘት ላይ ወደ \"%s\""

#: ../src/window.c:414
msgid ""
"A simple frontend to easily connect/mount to local and remote filesystems"
msgstr ""

#: ../src/window.c:415
msgid "Copyright 2008-2011 Enrico Tröger"
msgstr ""

#: ../src/window.c:418
msgid "translator-credits"
msgstr "ምስጋና-ለ ተርጓሚዎች"

#. Translators: This is a list of "protocol description (protocol)"
#: ../src/window.c:467
#, c-format
msgid "%s (%s)"
msgstr ""

#: ../src/window.c:472
msgid "Gigolo can use the following protocols provided by GVfs:"
msgstr ""

#: ../src/window.c:485 ../src/gigolo.ui.h:22
msgid "Supported Protocols"
msgstr "የ ተደገፉ አሰራሮች"

#: ../src/window.c:587 ../src/window.c:646
#, c-format
msgid "The command '%s' failed"
msgstr "ይህ ትእዛዝ '%s' ወድቋል"

#: ../src/window.c:588 ../src/window.c:621 ../src/window.c:634
#: ../src/window.c:647 ../src/window.c:844 ../src/bookmarkeditdialog.c:209
#: ../src/bookmarkeditdialog.c:228 ../src/bookmarkeditdialog.c:241
#: ../src/bookmarkeditdialog.c:252 ../src/bookmarkeditdialog.c:263
msgid "Error"
msgstr "ስህተት"

#: ../src/window.c:621
msgid "Invalid terminal command"
msgstr ""

#: ../src/window.c:633
#, c-format
msgid "No default location available for \"%s\""
msgstr ""

#: ../src/window.c:753
msgid "Edit _Bookmark"
msgstr ""

#: ../src/window.c:755 ../src/browsenetworkpanel.c:446
#: ../src/browsenetworkpanel.c:489
msgid "Create _Bookmark"
msgstr ""

#: ../src/window.c:1508
msgid "Connected"
msgstr "ተገናኝቷል"

#: ../src/window.c:1517 ../src/bookmarkdialog.c:310
msgid "Service Type"
msgstr "የ ግልጋሎት አይነት"

#: ../src/window.c:1525 ../src/bookmarkdialog.c:302
msgid "Name"
msgstr "ስም"

#: ../src/window.c:1590
msgid "Bookmarks"
msgstr ""

#: ../src/window.c:1635 ../src/window.c:1668 ../src/gigolo.ui.h:1
#: ../gigolo.desktop.in.h:1
msgid "Gigolo"
msgstr ""

#: ../src/settings.c:690
msgid "Move it now?"
msgstr "አሁን ላንቀሳቅሰው?"

#: ../src/settings.c:691
msgid "Gigolo needs to move your old configuration directory before starting."
msgstr ""

#: ../src/settings.c:699
#, c-format
msgid ""
"Your old configuration directory \"%s\" could not be moved to \"%s\" (%s). "
"Please move manually the directory to the new location."
msgstr ""

#: ../src/settings.c:702
msgid "Warning"
msgstr "ማስጠንቀቂያ"

#: ../src/bookmarkdialog.c:90
#, c-format
msgid "Domain: %s"
msgstr "ግዛት: %s"

#: ../src/bookmarkdialog.c:95
#, c-format
msgid "Share: %s"
msgstr "ማካፈያ: %s"

#: ../src/bookmarkdialog.c:101
#, c-format
msgid "Folder: %s"
msgstr "ፎልደር: %s"

#: ../src/bookmarkdialog.c:107
#, c-format
msgid "Path: %s"
msgstr "መንገድ: %s"

#: ../src/bookmarkdialog.c:318
msgid "Host"
msgstr "ጋባዥ"

#: ../src/bookmarkdialog.c:326
msgid "Port"
msgstr "Port"

#: ../src/bookmarkdialog.c:334
msgid "Auto-Connect"
msgstr "በራሱ መገናኛ"

#: ../src/bookmarkdialog.c:342
msgid "Username"
msgstr "የ ተጠቃሚ ስም"

#: ../src/bookmarkdialog.c:350
msgid "Other information"
msgstr "ሌላ መረጃ"

#: ../src/bookmarkdialog.c:371 ../src/bookmarkdialog.c:419
msgid "_Add"
msgstr ""

#: ../src/bookmarkdialog.c:378 ../src/bookmarkdialog.c:424
#: ../src/gigolo.ui.h:4
msgid "_Edit"
msgstr "_ማረሚያ"

#: ../src/bookmarkdialog.c:385 ../src/bookmarkdialog.c:429
msgid "_Delete"
msgstr "_ማጥፊያ"

#: ../src/bookmarkdialog.c:406
msgid "Edit Bookmarks"
msgstr ""

#: ../src/bookmarkeditdialog.c:210
msgid "You must enter a name for the bookmark."
msgstr ""

#: ../src/bookmarkeditdialog.c:229
msgid ""
"The entered bookmark name is already in use. Please choose another one."
msgstr ""

#: ../src/bookmarkeditdialog.c:242
msgid "You must enter a server address or name."
msgstr ""

#: ../src/bookmarkeditdialog.c:253
msgid "You must enter a share name."
msgstr ""

#: ../src/bookmarkeditdialog.c:264
msgid "You must enter a valid URI for the connection."
msgstr ""

#: ../src/bookmarkeditdialog.c:524
msgid "_Device:"
msgstr "_አካል:"

#: ../src/bookmarkeditdialog.c:526 ../src/bookmarkeditdialog.c:1018
msgid "_Server:"
msgstr "_ሰርቨር:"

#: ../src/bookmarkeditdialog.c:786
msgid "Create Bookmark"
msgstr ""

#: ../src/bookmarkeditdialog.c:793
msgid "Edit Bookmark"
msgstr ""

#: ../src/bookmarkeditdialog.c:801
msgid "Connect to Server"
msgstr "ወደ ሰርቨር መገናኛ"

#. Bookmark Settings
#: ../src/bookmarkeditdialog.c:955
msgid "Bookmark Settings"
msgstr ""

#: ../src/bookmarkeditdialog.c:961
msgid "_Name:"
msgstr ""

#: ../src/bookmarkeditdialog.c:970
msgid "_Color:"
msgstr "_ቀለም:"

#: ../src/bookmarkeditdialog.c:979
msgid "Au_to-Connect:"
msgstr ""

#. Connection Settings
#: ../src/bookmarkeditdialog.c:990
msgid "Connection Settings"
msgstr ""

#: ../src/bookmarkeditdialog.c:996
msgid "Service t_ype:"
msgstr ""

#: ../src/bookmarkeditdialog.c:1012
msgid "_Location (URI):"
msgstr ""

#: ../src/bookmarkeditdialog.c:1024
msgid "P_ath:"
msgstr "መ_ንገድ:"

#: ../src/bookmarkeditdialog.c:1030
msgid "_Share:"
msgstr "_ማካፈያ:"

#. Optional Information
#: ../src/bookmarkeditdialog.c:1049
msgid "Optional Information"
msgstr ""

#: ../src/bookmarkeditdialog.c:1055
msgid "_Port:"
msgstr "_Port:"

#: ../src/bookmarkeditdialog.c:1061
msgid "Set the port to 0 to use the default port"
msgstr ""

#: ../src/bookmarkeditdialog.c:1063
msgid "_Folder:"
msgstr "_ፎልደር:"

#: ../src/bookmarkeditdialog.c:1069
msgid ""
"This is not used for the actual mount, only necessary for opening the mount "
"point in a file browser"
msgstr ""

#: ../src/bookmarkeditdialog.c:1072
msgid "_Domain:"
msgstr "_ግዛት:"

#: ../src/bookmarkeditdialog.c:1078
msgid "_User Name:"
msgstr "የ _ተጠቃሚ ስም:"

#: ../src/preferencesdialog.c:146
msgid "Icons"
msgstr "ምልክቶች"

#: ../src/preferencesdialog.c:147
msgid "Text"
msgstr "ጽሁፍ"

#: ../src/preferencesdialog.c:148
msgid "Both"
msgstr "ሁለቱንም"

#: ../src/preferencesdialog.c:149
msgid "Both horizontal"
msgstr "ሁለቱንም በ አግድም"

#: ../src/preferencesdialog.c:168
msgid "Horizontal"
msgstr "በ አግድም"

#: ../src/preferencesdialog.c:169
msgid "Vertical"
msgstr "በ ቁመት"

#: ../src/preferencesdialog.c:188
msgid "Symbols"
msgstr "ምልክቶች"

#: ../src/preferencesdialog.c:189
msgid "Detailed List"
msgstr "ዝርዝር"

#: ../src/preferencesdialog.c:333
msgid "General"
msgstr "ባጠቃላይ"

#. Row
#: ../src/preferencesdialog.c:336
msgid "_File Manager"
msgstr "የ _ፋይል አስተዳዳሪ"

#: ../src/preferencesdialog.c:347
msgid "Enter the name of a program to use to open or view mount points"
msgstr ""

#. Row
#: ../src/preferencesdialog.c:354
msgid "_Terminal"
msgstr "_ተርሚናል"

#: ../src/preferencesdialog.c:365
msgid "Enter the name of a program to open mount points in a terminal"
msgstr ""

#. Row
#: ../src/preferencesdialog.c:372
msgid "_Bookmark Auto-Connect Interval"
msgstr ""

#: ../src/preferencesdialog.c:378
msgid ""
"How often to try auto connecting bookmarks, in seconds. Zero disables "
"checking."
msgstr ""

#: ../src/preferencesdialog.c:389
msgid "Interface"
msgstr "ገጽታ"

#: ../src/preferencesdialog.c:391
msgid "_Save window position and geometry"
msgstr ""

#: ../src/preferencesdialog.c:392
msgid "Saves the window position and geometry and restores it at the start"
msgstr ""

#: ../src/preferencesdialog.c:395
msgid "Show status _icon in the Notification Area"
msgstr ""

#: ../src/preferencesdialog.c:398
msgid "Start _minimized in the Notification Area"
msgstr ""

#: ../src/preferencesdialog.c:403
msgid "Show side panel"
msgstr "የ ጎን ክፍል ማሳያ"

#: ../src/preferencesdialog.c:404
msgid ""
"Whether to show a side panel for browsing the local network for available "
"Samba/Windows shares and a bookmark list"
msgstr ""

#: ../src/preferencesdialog.c:407
msgid "Show auto-connect error messages"
msgstr ""

#: ../src/preferencesdialog.c:408
msgid ""
"Whether to show error message dialogs when auto-connecting of bookmarks "
"fails"
msgstr ""

#: ../src/preferencesdialog.c:411
msgid "_Connection List Mode"
msgstr ""

#: ../src/preferencesdialog.c:427
msgid "Toolbar"
msgstr "እቃ መደርደሪያ"

#: ../src/preferencesdialog.c:429
msgid "Show _toolbar"
msgstr "_እቃ መደርደሪያ ማሳያ"

#: ../src/preferencesdialog.c:432
msgid "St_yle"
msgstr "ዘ_ዴ"

#: ../src/preferencesdialog.c:441
msgid "_Orientation"
msgstr "_አቅጣጫ"

#: ../src/preferencesdialog.c:496
msgid "Preferences"
msgstr "ምርጫዎች"

#: ../src/backendgvfs.c:215
msgid "No bookmark"
msgstr ""

#: ../src/backendgvfs.c:233
#, c-format
msgid ""
"<b>%s</b>\n"
"\n"
"URI: %s\n"
"Connected: Yes\n"
"Service Type: %s\n"
"Bookmark: %s"
msgstr ""

#: ../src/backendgvfs.c:248
#, c-format
msgid "<b>Unix device: %s</b>"
msgstr ""

#: ../src/backendgvfs.c:445 ../src/backendgvfs.c:484
msgid "unknown"
msgstr "ያልታወቀ"

#: ../src/backendgvfs.c:451 ../src/backendgvfs.c:559
#, c-format
msgid "Connecting to \"%s\" failed."
msgstr "መገናኘት ወደ \"%s\" ወድቋል"

#: ../src/backendgvfs.c:488
#, c-format
msgid "Disconnecting from \"%s\" failed."
msgstr ""

#: ../src/mountdialog.c:95
msgid "Connecting"
msgstr "በ መገናኘት ላይ"

#: ../src/browsenetworkpanel.c:183
msgid "No Workgroups found"
msgstr ""

#: ../src/browsenetworkpanel.c:194
msgid "No Shares found"
msgstr "ምንም ማካፈያ አልተገኘም"

#: ../src/browsenetworkpanel.c:199
msgid "No Hosts found"
msgstr ""

#: ../src/browsenetworkpanel.c:439 ../src/gigolo.ui.h:14
msgid "_Connect"
msgstr ""

#: ../src/browsenetworkpanel.c:482
msgid "Connect to the selected share"
msgstr ""

#: ../src/browsenetworkpanel.c:490
msgid "Create a bookmark from the selected share"
msgstr ""

#: ../src/browsenetworkpanel.c:500
msgid "Refresh the network list"
msgstr ""

#: ../src/browsenetworkpanel.c:512 ../src/bookmarkpanel.c:265
msgid "Close panel"
msgstr "ክፍል መዝጊያ"

#: ../src/bookmarkpanel.c:90
msgid "No bookmarks"
msgstr ""

#: ../src/bookmarkpanel.c:253
msgid "Connect to the selected bookmark"
msgstr ""

#: ../src/gigolo.ui.h:2
msgid "_File"
msgstr "_ፋይል"

#: ../src/gigolo.ui.h:3
msgid "_Quit"
msgstr "_ማጥፊያ"

#: ../src/gigolo.ui.h:5
msgid "_Edit Bookmarks"
msgstr ""

#: ../src/gigolo.ui.h:6
msgid "_Preferences"
msgstr ""

#: ../src/gigolo.ui.h:7
msgid "_View"
msgstr "_መመልከቻ"

#: ../src/gigolo.ui.h:8
msgid "_Toolbar"
msgstr "_እቃ መደርደሪያ"

#: ../src/gigolo.ui.h:9
msgid "Side _Panel"
msgstr "የ ጎን _ክፍል"

#: ../src/gigolo.ui.h:10
msgid "Status _Icon"
msgstr "የ ሁኔታዎች _ምልክት"

#: ../src/gigolo.ui.h:11
msgid "View as _Symbols"
msgstr "እንደ _ምልክት መመልከቻ"

#: ../src/gigolo.ui.h:12
msgid "View as _Detailed List"
msgstr ""

#: ../src/gigolo.ui.h:13
msgid "_Actions"
msgstr "_ተግባሮች"

#: ../src/gigolo.ui.h:15
msgid "_Disconnect"
msgstr ""

#: ../src/gigolo.ui.h:16
msgid "_Bookmarks"
msgstr ""

#: ../src/gigolo.ui.h:17
msgid "_Open"
msgstr "_መክፈቻ "

#: ../src/gigolo.ui.h:18
msgid "Open in _Terminal"
msgstr "በ _ተርሚናል ውስጥ መክፈቻ"

#: ../src/gigolo.ui.h:19
msgid "_Copy URI"
msgstr ""

#: ../src/gigolo.ui.h:20
msgid "_Help"
msgstr "_እርዳታ"

#: ../src/gigolo.ui.h:21
msgid "Online Help"
msgstr "በ መስመር ላይ እርዳታ"

#: ../src/gigolo.ui.h:23
msgid "About"
msgstr ""

#: ../src/gigolo.ui.h:24
msgid "Choose a bookmark to connect to"
msgstr ""

#: ../src/gigolo.ui.h:25
msgid "Disconnect the selected resource"
msgstr ""

#: ../src/gigolo.ui.h:26
msgid "Open the bookmark manager to add, edit or delete bookmarks"
msgstr ""

#: ../src/gigolo.ui.h:27
msgid "Open the selected resource with a file manager"
msgstr ""

#: ../src/gigolo.ui.h:28
msgid "Start a terminal from here"
msgstr ""

#: ../src/gigolo.ui.h:29
msgid "Open _Terminal"
msgstr ""

#: ../src/gigolo.ui.h:30
msgid "Quit Gigolo"
msgstr ""

#: ../src/gigolo.ui.h:31
msgid "_Edit Bookmark"
msgstr ""

#: ../gigolo.desktop.in.h:2
msgid "Frontend for GIO/GVfs"
msgstr ""

#: ../gigolo.desktop.in.h:3
msgid "A simple frontend to easily connect to remote filesystems"
msgstr ""
